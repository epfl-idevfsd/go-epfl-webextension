// console.log("go.js loaded....")
$(function () {
  // Set the focus in the alias input
  $('#alias').focus()

  // Set the current URL
  browser.tabs.query({ currentWindow: true, active: true }).then(function (tabs) {
    $('#url').val(tabs[0].url)
  }).catch(console.error)

  // POST when the shorten button is clicked
  $('#shorten').click(async function () {
    await postToGO()
  })

  // POST when the form is submitted (i.e. hit enter)
  $('#frm_goaddon').submit(async function (event) {
    event.preventDefault()
    await postToGO()
  })

  // Handle the POST to go.epfl.ch
  async function postToGO () {
    let token = await browser.storage.sync.get('goToken')
    // console.debug(token)
    $.post({
      url: 'https://go.epfl.ch/api/v1/alias',
      data: { alias: $('#alias').val(), url: $('#url').val(), token }
    })
    .done(async function (data) {
      // console.debug("DONE")
      // console.debug(data)
      if (data.status == 'created') {
        createdLink = 'https://go.epfl.ch/' + $('#alias').val()
        $('#linkToCopy').val(createdLink)
        let autoCopy = await browser.storage.sync.get('goAutoCopy')
        if (autoCopy.goAutoCopy === true) {
          $('#message').html('<span class="green">Shortened and copied!</span><br><a id="newLink" href="' + createdLink + '" target="_blank">' + createdLink + '</a>')
          copy()
        } else {
          $('#message').html('<span class="green">Shortened!</span><br><a id="newLink" href="' + createdLink + '" target="_blank">' + createdLink + '</a> <button id="copyBtn" type="button">copy</button><br>')
          $("#copyBtn").on("click", copy)
        }

        browser.runtime.sendMessage('Shortened! https://go.epfl.ch/' + $('#alias').val())
      }
    })
    .fail(function (xhr, status, error) {
      // console.debug(xhr)
      // console.debug("STATUS")
      // console.debug(xhr.responseJSON.status)
      // console.debug(status)
      // console.debug("ERRORS")
      // console.debug(xhr.responseJSON.errors)
      // console.debug(error)
      let errmsg = ''
      if (typeof xhr.responseJSON.errors != 'undefined') {
        if (typeof xhr.responseJSON.errors.alias != 'undefined') {
          errmsg += xhr.responseJSON.errors.alias[0]
        }
        if (typeof xhr.responseJSON.errors.url != 'undefined') {
          errmsg += xhr.responseJSON.errors.url[0]
        }
      }
      if (typeof xhr.responseJSON.status != 'undefined' && xhr.responseJSON.status == 'forbidden') {
        errmsg += xhr.responseJSON.message
      }
      $('#message').html('<span class="red">' + status + '!</span><br>' + errmsg + '<br>')
      browser.runtime.sendMessage('Fail to create https://go.epfl.ch/' + $('#alias').val() + '! (' + status + ' — ' + errmsg + ')')
    })
  }

  // Open the option page
  $('#options').click(() => {
    var openingPage = browser.runtime.openOptionsPage()
    openingPage.then(onOpened, onError)
  })
  function onOpened () {
    // console.log(`Options page opened`)
  }
  function onError (error) {
    console.log(`Error: ${error}`)
  }

  // Randomiye the alias generation
  $('#randomizeAlias').click(async () => {
    randomizeAlias()
    let autoSubmit = await browser.storage.sync.get('goRandom')
    // console.debug(autoSubmit.goRandom)
    if (autoSubmit.goRandom === true) {
      await postToGO()
    }
  })
  // https://stackoverflow.com/a/26410127
  // str byteToHex (uint8 byte)
  //   converts a single byte to a hex string
  function byteToHex (byte) {
    return ('0' + byte.toString(16)).slice(-2)
  }
  // str generateId (int len)
  //   len - must be an even number (default: 40)
  function generateId (len = 40) {
    var arr = new Uint8Array(len / 2)
    window.crypto.getRandomValues(arr)
    return Array.from(arr, byteToHex).join('')
  }
  function randomizeAlias () {
    $('#alias').val(generateId(6))
  }

  function copy() {
    $('#linkToCopy').select()
    document.execCommand("copy")
  }
})
